import requests

app_id = 'f40b1bef'
app_key = '52faf4b2e672d3bef2176a89b956ba4e'

def search_recette(ingredient, meal_type, cuisine_type):
    # Construisons l'URL de l'API en fonction de l'ingrédient et du type de plat et le type de cuisine spécifiés
    url = f"https://api.edamam.com/search?q={ingredient}&mealType={meal_type}&cuisineType={cuisine_type}&app_id={app_id}&app_key={app_key}"

    # Effectuons une requête HTTP GET à l'API
    response = requests.get(url)

    # Analysons la réponse JSON de l'API
    data = response.json()

    # Parcourez les recettes trouvées et affichez les informations sur les ingrédients, les instructions et les temps de cuisson
    for hit in data["hits"]:
        recipe = hit["recipe"]
        
        return recipe


def search_recette_by_country(country):
    # Construisons l'URL de l'API en fonction du type de cuisine spécifiés
    url = f"https://api.edamam.com/search?q={country}&app_id={app_id}&app_key={app_key}"

    # Effectuons une requête HTTP GET à l'API
    response = requests.get(url)

    # Analysons la réponse JSON de l'API
    data = response.json()

    # Parcourez les recettes trouvées et affichez les informations sur les ingrédients, les instructions et les temps de cuisson
    for hit in data["hits"]:
        recipe = hit["recipe"]
        
        return recipe

def search_recette_by_time(ingredient, total_time):
    # Construisons l'URL de l'API en fonction du temps de cuisson spécifiés
    url = f"https://api.edamam.com/search?q={ingredient}&time={total_time}&app_id={app_id}&app_key={app_key}"

    # Effectuons une requête HTTP GET à l'API
    response = requests.get(url)

    # Analysons la réponse JSON de l'API
    data = response.json()

    # Parcourez les recettes trouvées et affichez les informations sur les ingrédients, les instructions et les temps de cuisson
    for hit in data["hits"]:
        recipe = hit["recipe"]
        
        return recipe
