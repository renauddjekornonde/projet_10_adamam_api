import unittest
import main
class TestRecherche(unittest.TestCase):
    def setUp(self):
        pass #Permet de dire au compilateur de sauter la fonction
    
    def test_recette(self):
         # Spécifions l'ingrédient 
        ingredient = "chicken"
        #Spécifions le Type de plat
        meal_type = "lunch"
        #Spécifions le Type de cuisine
        cuisine_type= "french"
        resultat= main.search_recette(ingredient, meal_type, cuisine_type)
        self.assertIn("Catalan Chicken", resultat["label"])
        
    def test_ingredient(self):
         # Spécifions l'ingrédient 
        ingredient = "chicken"
        #Spécifions le Type de plat
        meal_type = "lunch"
        #Spécifions le Type de cuisine
        cuisine_type= "french"
        ingredientRecette=[]
        recette= main.search_recette(ingredient, meal_type, cuisine_type)
        for ingredient in recette["ingredients"]:
            ingredientRecette.append(ingredient["text"])
        self.assertIn("8 slices bacon", ingredientRecette)
        self.assertNotIn("piment", ingredientRecette)
        
    def test_country_cuisson(self):
        #Spécifions le Type de cuisine
        cuisine_type= "french"
        countries=[]
        recetteCountry= main.search_recette_by_country(cuisine_type)
        for country in recetteCountry["cuisineType"]:
            countries.append(country)
        self.assertNotIn("Tchad", countries)
        self.assertIn("french", countries)
        
    def test_time_cuisson(self):
        #Spécifions l'ingredient et le temps de cuisine
        total_time= 60
        ingredient= "chicken"
        recetteTime= main.search_recette_by_time(ingredient,total_time)
        
        self.assertNotEqual(recetteTime["totalTime"], 70.0)
        self.assertEqual(recetteTime["totalTime"], 60.0)
        
if __name__== '__main__':
    unittest.main()
